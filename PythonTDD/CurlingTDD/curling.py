class OneTeam:
    def __init__(self):
        self.distance = 100
        self.distance_from_center = 0
        self.rolls = [] 

    def score(self, distancetraveled):
        self.distance_from_center = abs(self.distance - distancetraveled)
        self.rolls.append(self.distance_from_center)
        
    def points(self):
        return self.distance_from_center

    def roll(self):
        self.rolls.sort()
        return self.rolls[0]
    
class TwoTeams:
    def __init__(self):
        self.rollsred = []
        self.rollsblue = []
        self.distance = 100
        self.distance_from_center = 0

    def red(self, distancetraveled):
        self.distance_from_center = abs(self.distance - distancetraveled)
        self.rollsred.append(self.distance_from_center)

    def blue(self, distancetraveled):
        self.distance_from_center = abs(self.distance - distancetraveled)
        self.rollsblue.append(self.distance_from_center)
    
    def winner(self):
        self.rollsblue.sort()
        self.rollsred.sort()
        
        if self.rollsblue[0] == self.rollsred[0]:
            return "Tie"
        elif self.rollsred[0] > self.rollsblue[0]:
            return "Blue"
        else: 
            return "Red"
        