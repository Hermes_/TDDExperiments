class Game:
    def __init__(self):
        self.points = [0,1,2,3,4,5,6,7,8,9,10]
        self.score = 0 

    def shoot(self, ring):
        self.score += self.points[ring]

    def point(self):
        return self.score
